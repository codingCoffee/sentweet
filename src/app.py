from flask            import Flask, redirect, jsonify
from flasgger         import Swagger
from social.twitter   import get_tweets_of_user, get_tweets_with_hashtag
from models.sentiment import train_sentiments, test_sentiments, sentiment_is_positive


app = Flask(__name__)
Swagger(app)


@app.route("/")
def index():
    """Redirect to the documentation"""
    return redirect('/apidocs')


@app.route("/train")
def train():
    # Train on existing data and print the output on the console and on the front end
    ret = train_sentiments()
    return jsonify(ret)


@app.route("/test")
def test():
    # Run the test functon and print the output on the console and on the front end
    ret = test_sentiments()
    return jsonify(ret)


@app.route("/sample/<text>")
def sample(text):
    return jsonify( sentiment_is_positive(text) )

@app.route("/twitter_user/<username>")
def user(username):
    # Fetch the users top 10 tweets
    tweets = get_tweets_of_user(username)
    return analyze(tweets)


@app.route("/twitter_hastag/<hashtag>")
def hashtag(hashtag):
    # Fetch the top 10 tweets with the hashtag
    tweets = get_tweets_with_hashtag(username)
    return analyze(tweets)


def analyze(tweets):
    # Test all tweets
    sentiments = {}
    for id, tweet in enumerate(tweets):
        sentiments[id] = {}
        sentiments[id]['positive'] = sentiment_is_positive(tweet)
        sentiments[id]['text'] = tweet

    # Return to front end
    return jsonify(sentiments)


if __name__ == '__main__':
    app.run(debug=True)

