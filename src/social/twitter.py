import tweepy
from tweepy.error import TweepError
import json

consumer_key        = "lmQiK09xX79TZlEReoGuygUJk"
consumer_secret     = "4zIUQu465Eywb7CnHIEHZ7NWYMNhywXZIEKvWTQRT7Y5uhd3Tu"
access_token        = "523264272-HRQVXqif3LjmomiPbN2ooUgs0GxyDAxJyy5LembD"
access_token_secret = "wwLXQmtxxrpq72NlPY8oHus5aHHYLrnIEsAZxyVLhyH1d"
# Authenticate twitter Api
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token,  access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True)


def get_tweets_of_user(username, no_of_tweets=10):
    """
    Fetches and returns a list of tweets tweeted by a user

    Parameters:
    username     (str): username the person who's tweets need to be fetched
    no_of_tweets (int): number of tweets to be returned

    Returns:
    list: of strings, containing the tweets
    """
    try:
        tweet_cursor = api.user_timeline(screen_name=username, count=no_of_tweets)
    except TweepError:
        # Catch error in case a given user doesn't exist
        tweet_cursor = []

    tweets = [tweet.text for tweet in tweet_cursor]
    return tweets

def get_tweets_with_hashtag(hashtag, no_of_tweets=10):
    """
    Fetches and returns a list of tweets containing a hashtag

    Parameters:
    hashtag      (str): username the person who's tweets need to be fetched
    no_of_tweets (int): number of tweets to be returned

    Returns:
    list: of strings, containing the tweets
    """
    tweet_cursor = tweepy.Cursor(api.search, q=hashtag, lang="en", result_type="recent").items(no_of_tweets)

    tweets = [tweet.text for tweet in tweet_cursor]
    return tweets
